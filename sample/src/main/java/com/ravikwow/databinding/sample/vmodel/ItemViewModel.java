package com.ravikwow.databinding.sample.vmodel;

import androidx.databinding.Observable;
import androidx.databinding.ObservableField;

import com.ravikwow.databinding.adapter.RVAdapterViewModel;
import com.ravikwow.databinding.sample.model.ItemModel;

import java.util.Objects;

/**
 * Created by ravikwow
 * Date: 04.11.2016.
 */
public class ItemViewModel extends RVAdapterViewModel<ItemModel> {
    public final ObservableField<String> number = new ObservableField<>();
    public final ObservableField<String> id = new ObservableField<>();

    {
        Observable.OnPropertyChangedCallback callback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == number) {
                    getModel().setNumber(Integer.parseInt(Objects.requireNonNull(number.get())));
                }
            }
        };
        number.addOnPropertyChangedCallback(callback);
    }

    @Override
    public void bind(ItemModel itemModel, int position) {
        super.bind(itemModel, position);
        this.number.set(Integer.toString(itemModel.getNumber()));
        this.id.set(Long.toString(itemModel.getId()));
    }
}
