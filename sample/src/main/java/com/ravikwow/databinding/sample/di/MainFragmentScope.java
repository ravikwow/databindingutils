package com.ravikwow.databinding.sample.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ravikwow
 * Date: 15.03.2019.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainFragmentScope {
}
