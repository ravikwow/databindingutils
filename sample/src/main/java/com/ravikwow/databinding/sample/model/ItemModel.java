package com.ravikwow.databinding.sample.model;

import androidx.core.util.ObjectsCompat;

/**
 * Created by ravikwow
 * Date: 04.11.2016.
 */
public class ItemModel {
    private int number;
    private long id;

    public ItemModel(long id, int number) {
        this.number = number;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;

    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemModel itemModel = (ItemModel) o;

        if (number != itemModel.number) return false;
        return id == itemModel.id;

    }

    @Override
    public int hashCode() {
        return ObjectsCompat.hash(id, number);
    }
}
