package com.ravikwow.databinding.sample.repository;

import androidx.core.util.ObjectsCompat;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by ravikwow
 * Date: 29.03.19.
 */
@Entity
public class Number {
    @PrimaryKey
    private long id;
    private int number;

    public Number(long id, int number) {
        this.id = id;
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Number number1 = (Number) o;
        return id == number1.id &&
                number == number1.number;
    }

    @Override
    public int hashCode() {
        return ObjectsCompat.hash(id, number);
    }
}
