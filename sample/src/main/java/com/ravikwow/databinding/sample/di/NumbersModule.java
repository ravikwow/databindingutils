package com.ravikwow.databinding.sample.di;

import android.content.Context;

import androidx.lifecycle.LifecycleOwner;
import androidx.room.Room;

import com.ravikwow.databinding.sample.adapter.NumbersAdapter;
import com.ravikwow.databinding.sample.adapter.NumbersDataSourceAdapter;
import com.ravikwow.databinding.sample.repository.AppDatabase;
import com.ravikwow.databinding.sample.repository.NumberDao;
import com.ravikwow.databinding.sample.usecase.NumbersUC;
import com.ravikwow.databinding.sample.vmodel.FragmentListVM;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ravikwow
 * Date: 15.03.2019.
 */
@Module(includes = ContextModule.class)
public class NumbersModule {

    private FragmentListVM fragmentListVM;
    private LifecycleOwner lifecycleOwner;

    public NumbersModule(FragmentListVM fragmentListVM, LifecycleOwner lifecycleOwner) {
        this.fragmentListVM = fragmentListVM;
        this.lifecycleOwner = lifecycleOwner;
    }

    @MainFragmentScope
    @Provides
    public NumbersAdapter numbersAdapter() {
        return new NumbersAdapter(fragmentListVM, lifecycleOwner);
    }

    @MainFragmentScope
    @Provides
    public NumbersDataSourceAdapter numbersDataSourceAdapter() {
        return new NumbersDataSourceAdapter(fragmentListVM, lifecycleOwner);
    }

    @MainFragmentScope
    @Provides
    public NumberDao NumberDao(Context context) {
        AppDatabase db = Room.databaseBuilder(context, AppDatabase.class, "db")
                .build();
        return db.numberDao();
    }

    @MainFragmentScope
    @Provides
    public NumbersUC numbersUC(NumberDao numberDao) {
        return new NumbersUC(numberDao);
    }
}
