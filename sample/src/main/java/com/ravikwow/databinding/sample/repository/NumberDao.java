package com.ravikwow.databinding.sample.repository;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Created by ravikwow
 * Date: 29.03.19.
 */
@Dao
public abstract class NumberDao {
    private static ArrayList<Number> getRandomNumbers(int count) {
        HashSet<Long> ids = new HashSet<>();
        ArrayList<Number> itemModels = new ArrayList<>();
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < count; i++) {
            long id;
            do {
                id = random.nextInt(count * 2);
            } while (ids.contains(id));
            ids.add(id);
            itemModels.add(new Number(id, random.nextInt(count * 5)));
        }
        return itemModels;
    }

    @Query("SELECT * FROM Number")
    public abstract LiveData<List<Number>> get();

    @Query("SELECT * FROM Number WHERE CAST(number AS TEXT) LIKE :str || '%' ORDER BY id ASC")
    public abstract DataSource.Factory<Integer, Number> getDataSource(String str);

    @Query("SELECT * FROM Number WHERE id = :id")
    public abstract LiveData<Number> get(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insert(Number number);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long[] insert(Collection<Number> numbers);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract int update(Number number);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract int update(Collection<Number> numbers);

    @Delete
    public abstract void delete(Number number);

    @Delete
    public abstract void delete(List<Number> numbers);

    @Query("DELETE FROM Number WHERE id not in (:ids)")
    public abstract void deleteNotIn(long[] ids);

    @Query("SELECT COUNT(*) FROM Number")
    public abstract int getCount();

    @Transaction
    public void generate(int count) {
        Collection<Number> numbers = getRandomNumbers(count);
        long[] ids = new long[count];
        int i = 0;
        for (Number number : numbers) {
            ids[i++] = number.getId();
        }
        int max = 500;
        if (ids.length > max) {
            for (int part = 0; part <= ids.length / max; part++) {
                int from = part * max;
                if (from > ids.length) break;
                int to = (part + 1) * max;
                if (to > ids.length) to = ids.length;
                long[] idsPart = Arrays.copyOfRange(ids, from, to);
                deleteNotIn(idsPart);
            }
        } else {
            deleteNotIn(ids);
        }
        insert(numbers);
    }

    @Transaction
    public void fillIfEmpty(int count) {
        if (getCount() == 0) {
            generate(count);
        }
    }
}
