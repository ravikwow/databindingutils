package com.ravikwow.databinding.sample.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.sample.adapter.NumbersDataSourceAdapter;
import com.ravikwow.databinding.sample.databinding.FragmentListBinding;
import com.ravikwow.databinding.sample.di.ContextModule;
import com.ravikwow.databinding.sample.di.DaggerFragmentsComponent;
import com.ravikwow.databinding.sample.di.NumbersModule;
import com.ravikwow.databinding.sample.usecase.NumbersUC;
import com.ravikwow.databinding.sample.vmodel.FragmentListVM;
import com.ravikwow.databinding.ui.ToolbarFragment;

import javax.inject.Inject;

public class NumbersDataSourceFragment extends ToolbarFragment<FragmentListBinding, FragmentListVM> {
    @Inject
    protected NumbersDataSourceAdapter adapter;
    @Inject
    protected NumbersUC numbersUC;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerFragmentsComponent.builder()
                .contextModule(new ContextModule(getContext()))
                .numbersModule(new NumbersModule(viewModel, this))
                .build().inject(this);
        numbersUC.fillIfEmpty();
        adapter.submitDataSource(numbersUC.reloadDataSource());
        viewModel.getRefreshEvent().observe(this, aVoid -> numbersUC.generate(o -> {
            adapter.submitDataSource(numbersUC.reloadDataSource());
        }));
        viewModel.getSearchEvent().observe(this, search -> {
            adapter.submitDataSource(numbersUC.getDataSource(search));
        });
        viewModel.getItemClickEvent().observe(this, itemModel -> {
            Toast.makeText(getContext(), getString(R.string.item_click, itemModel.getNumber()), Toast.LENGTH_SHORT).show();
        });
        viewModel.getItemLongClickEvent().observe(this, itemModel -> {
            int oldNumber = itemModel.getNumber();
            int newNumber = numbersUC.setRandom(itemModel.getId());
            Toast.makeText(getContext(), getString(R.string.item_long_click, oldNumber, newNumber), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recyclerView.setAdapter(adapter);
    }
}
