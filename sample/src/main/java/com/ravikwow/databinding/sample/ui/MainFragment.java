package com.ravikwow.databinding.sample.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.sample.databinding.FragmentMainBinding;
import com.ravikwow.databinding.sample.vmodel.FragmentMainVM;
import com.ravikwow.databinding.ui.ToolbarFragment;

/**
 * Created by ravikwow
 * Date: 12.11.2016.
 */
public class MainFragment extends ToolbarFragment<FragmentMainBinding, FragmentMainVM> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel.getStartNumbersDataSourcEvent().observe(this, aVoid -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment fragment = new NumbersDataSourceFragment();
            ft.replace(R.id.container, fragment, fragment.getClass().getName());
            ft.addToBackStack(fragment.getClass().getName());
            ft.commitAllowingStateLoss();
        });
        viewModel.getStartNumbersEvent().observe(this, aVoid -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment fragment = new NumbersFragment();
            ft.replace(R.id.container, fragment, fragment.getClass().getName());
            ft.addToBackStack(fragment.getClass().getName());
            ft.commitAllowingStateLoss();
        });
        viewModel.getStartNumbersKEvent().observe(this, aVoid -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment fragment = new NumbersFragmentK();
            ft.replace(R.id.container, fragment, fragment.getClass().getName());
            ft.addToBackStack(fragment.getClass().getName());
            ft.commitAllowingStateLoss();
        });
    }
}
