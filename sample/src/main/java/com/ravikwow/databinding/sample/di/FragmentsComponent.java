package com.ravikwow.databinding.sample.di;

import com.ravikwow.databinding.sample.ui.NumbersDataSourceFragment;
import com.ravikwow.databinding.sample.ui.NumbersFragment;

import dagger.Component;

/**
 * Created by ravikwow
 * Date: 15.03.2019.
 */
@MainFragmentScope
@Component(modules = NumbersModule.class)
public interface FragmentsComponent {
    void inject(NumbersFragment numbersFragment);

    void inject(NumbersDataSourceFragment numbersDataSourceFragment);
}
