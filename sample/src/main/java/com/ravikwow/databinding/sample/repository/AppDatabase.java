package com.ravikwow.databinding.sample.repository;

import androidx.room.Database;
import androidx.room.RoomDatabase;

/**
 * Created by ravikwow
 * Date: 29.03.19.
 */
@Database(entities = {Number.class}, version = AppDatabase.VERSION)
public abstract class AppDatabase extends RoomDatabase {
    static final int VERSION = 1;

    public abstract NumberDao numberDao();
}
