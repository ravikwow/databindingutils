package com.ravikwow.databinding.sample.adapter;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DiffUtil;

import com.ravikwow.databinding.adapter.RVAdapterViewHolder;
import com.ravikwow.databinding.adapter.RecyclerViewDataSourceAdapter;
import com.ravikwow.databinding.sample.BR;
import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.sample.model.ItemModel;
import com.ravikwow.databinding.sample.vmodel.FragmentListVM;
import com.ravikwow.databinding.sample.vmodel.ItemViewModel;

import java.util.List;

public class NumbersDataSourceAdapter extends RecyclerViewDataSourceAdapter<ItemModel, ItemViewModel> {
    private final FragmentListVM fragmentListVM;

    public NumbersDataSourceAdapter(FragmentListVM fragmentListVM, LifecycleOwner lifecycleOwner) {
        super(new DiffUtil.ItemCallback<ItemModel>() {
            @Override
            public boolean areItemsTheSame(@NonNull ItemModel oldItem, @NonNull ItemModel newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull ItemModel oldItem, @NonNull ItemModel newItem) {
                return oldItem.equals(newItem);
            }

            @NonNull
            @Override
            public Object getChangePayload(@NonNull ItemModel oldItem, @NonNull ItemModel newItem) {
                return newItem;
            }
        }, lifecycleOwner, 20, R.layout.list_item);
        this.fragmentListVM = fragmentListVM;
    }

    @Override
    public void onBindViewHolder(@NonNull RVAdapterViewHolder<ItemModel, ItemViewModel> holder, int position, @NonNull List<Object> payloads) {
        holder.binding.setVariable(BR.fragmentListVM, fragmentListVM);
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            ItemModel model = (ItemModel) payloads.get(payloads.size() - 1);
            holder.getVm().number.set(Integer.toString(model.getNumber()));
        }
    }
}
