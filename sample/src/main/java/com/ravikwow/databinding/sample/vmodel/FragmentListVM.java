package com.ravikwow.databinding.sample.vmodel;

import android.app.Application;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.databinding.Observable;
import androidx.databinding.ObservableField;

import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.sample.model.ItemModel;
import com.ravikwow.databinding.utils.SingleLiveEvent;
import com.ravikwow.databinding.vm.ToolbarFragmentVM;

/**
 * Created by ravikwow
 * Date: 15.11.16.
 */
public class FragmentListVM extends ToolbarFragmentVM {
    public final ObservableField<String> searchText = new ObservableField<>();
    private final SingleLiveEvent<Void> refreshEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<String> searchEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<ItemModel> itemClickEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<ItemModel> itemLongClickEvent = new SingleLiveEvent<>();

    public FragmentListVM(@NonNull Application application) {
        super(application);
        title.set(R.string.app_name);
        menu.set(R.menu.main);
        searchText.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                searchEvent.setValue(searchText.get());
            }
        });
    }

    public SingleLiveEvent<ItemModel> getItemLongClickEvent() {
        return itemLongClickEvent;
    }

    public SingleLiveEvent<ItemModel> getItemClickEvent() {
        return itemClickEvent;
    }

    public SingleLiveEvent<String> getSearchEvent() {
        return searchEvent;
    }

    public SingleLiveEvent<Void> getRefreshEvent() {
        return refreshEvent;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                refreshEvent.call();
                return true;
            default:
                return super.onMenuItemClick(item);
        }
    }

    public void itemClick(ItemModel itemModel) {
        itemClickEvent.postValue(itemModel);
    }

    public boolean itemLongClick(ItemModel itemModel) {
        itemLongClickEvent.postValue(itemModel);
        return true;
    }
}
