package com.ravikwow.databinding.sample.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import androidx.room.Room
import com.ravikwow.databinding.sample.R
import com.ravikwow.databinding.sample.model.ItemModel
import com.ravikwow.databinding.sample.repository.AppDatabase
import com.ravikwow.databinding.sample.usecase.NumbersUC
import com.ravikwow.databinding.ui.LogLifeCycleFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow

class NumbersFragmentK : LogLifeCycleFragment() {
    private val numbersUC: NumbersUC by lazy {
        NumbersUC(
            context?.let {
                Room.databaseBuilder(it, AppDatabase::class.java, "db").build().numberDao()
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent { CreateVeiw() }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun PreviewVeiw() {
        CreateVeiw()
    }

    @Composable
    fun CreateVeiw() {
        val colors = darkColors(
            primary = colorResource(id = R.color.colorPrimary),
            primaryVariant = colorResource(id = R.color.colorPrimaryDark),
            onPrimary = colorResource(id = R.color.colorPrimary),
            secondary = colorResource(id = R.color.colorAccent),
            onSecondary = colorResource(id = R.color.colorAccent),
        )
        var searchText by remember { mutableStateOf(TextFieldValue("")) }
        val flowPagerData = Pager(
            PagingConfig(20), 1,
            numbersUC.getDataSourceFactory(searchText.text).asPagingSourceFactory(Dispatchers.IO)
        ).flow
        numbersUC.fillIfEmpty()
        MaterialTheme(
            colors = colors
        ) {
            Column {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    @Suppress("DEPRECATION")
                    TextField(
                        value = searchText,
                        onValueChange = {
                            searchText = it
                        },
                        modifier = Modifier.weight(1f),
                        label = { Text(text = stringResource(id = R.string.search)) },
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = Color(0x00000000),
                            textColor = colorResource(id = android.R.color.secondary_text_dark)
                        )
                    )
                    IconButton(onClick = { numbersUC.generate { } }) {
                        @Suppress("DEPRECATION")
                        Icon(
                            painter = painterResource(id = android.R.drawable.stat_notify_sync_noanim),
                            contentDescription = null,
                            tint = colorResource(id = android.R.color.secondary_text_dark)
                        )
                    }
                }
                CreateList(flowPagerData)
            }
        }
    }

    @Suppress("OPT_IN_IS_NOT_ENABLED")
    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun CreateList(flowPagerData: Flow<PagingData<ItemModel>>) {
        val list = flowPagerData.collectAsLazyPagingItems()
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            items(items = list, key = { it.id }) { item ->
                Row(
                    modifier = Modifier
                        .combinedClickable(
                            onClick = {
                                Toast
                                    .makeText(
                                        context,
                                        getString(R.string.item_click, item?.number),
                                        Toast.LENGTH_SHORT
                                    )
                                    .show()
                            },
                            onLongClick = {
                                item?.let {
                                    val oldNumber: Int = it.number
                                    val newNumber = numbersUC.setRandom(it.id)
                                    Toast
                                        .makeText(
                                            context,
                                            getString(
                                                R.string.item_long_click,
                                                oldNumber,
                                                newNumber
                                            ),
                                            Toast.LENGTH_SHORT
                                        )
                                        .show()
                                }
                            })
                        .animateItemPlacement()
                        .padding(start = Dp(12f), end = Dp(12f))
                        .fillMaxWidth()
                        .height(height = Dp(48f)),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    @Suppress("DEPRECATION")
                    Text(
                        text = stringResource(id = R.string.id),
                        color = colorResource(id = android.R.color.secondary_text_dark)
                    )
                    @Suppress("DEPRECATION")
                    Text(
                        text = item?.id?.toString() ?: "",
                        modifier = Modifier
                            .width(width = Dp(48f))
                            .wrapContentHeight(),
                        color = colorResource(id = android.R.color.secondary_text_dark)
                    )
                    @Suppress("DEPRECATION")
                    Text(
                        text = stringResource(id = R.string.number),
                        color = colorResource(id = android.R.color.secondary_text_dark)
                    )
                    @Suppress("DEPRECATION")
                    Text(
                        text = item?.number?.toString() ?: "",
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight(),
                        color = colorResource(id = android.R.color.secondary_text_dark)
                    )
                }
            }
        }
    }
}
