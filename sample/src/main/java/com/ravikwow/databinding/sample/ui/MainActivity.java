package com.ravikwow.databinding.sample.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.sample.databinding.ActivityMainBinding;
import com.ravikwow.databinding.ui.DataBindingActivity;
import com.ravikwow.databinding.ui.VM;

/**
 * Created by ravikwow
 * Date: 12.11.2016.
 */
public class MainActivity extends DataBindingActivity<ActivityMainBinding, VM> {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new MainFragment();
        ft.replace(R.id.container, fragment, fragment.getClass().getName());
        ft.commitAllowingStateLoss();
    }
}
