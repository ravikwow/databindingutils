package com.ravikwow.databinding.sample.adapter;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DiffUtil;

import com.ravikwow.databinding.adapter.RVAdapterViewHolder;
import com.ravikwow.databinding.adapter.RecyclerViewAdapter;
import com.ravikwow.databinding.sample.BR;
import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.sample.model.ItemModel;
import com.ravikwow.databinding.sample.vmodel.FragmentListVM;
import com.ravikwow.databinding.sample.vmodel.ItemViewModel;

import java.util.List;

/**
 * Created by ravikwow
 * Date: 15.11.16.
 */
public class NumbersAdapter extends RecyclerViewAdapter<ItemModel, ItemViewModel> {
    private final FragmentListVM fragmentListVM;

    public NumbersAdapter(FragmentListVM fragmentListVM, LifecycleOwner lifecycleOwner) {
        super(new DiffUtil.ItemCallback<ItemModel>() {
            @Override
            public boolean areItemsTheSame(@NonNull ItemModel oldItem, @NonNull ItemModel newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull ItemModel oldItem, @NonNull ItemModel newItem) {
                return oldItem.equals(newItem);
            }

            @NonNull
            @Override
            public Object getChangePayload(@NonNull ItemModel oldItem, @NonNull ItemModel newItem) {
                return newItem;
            }
        }, lifecycleOwner, R.layout.list_item);
        setComparator((left, right) -> (int) (left.getId() - right.getId()));
        this.fragmentListVM = fragmentListVM;
    }

    @Override
    public void onBindViewHolder(RVAdapterViewHolder<ItemModel, ItemViewModel> holder, int position, List<Object> payloads) {
        holder.binding.setVariable(BR.fragmentListVM, fragmentListVM);
        if (payloads == null || payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            ItemModel model = (ItemModel) payloads.get(payloads.size() - 1);
            holder.getVm().number.set(Integer.toString(model.getNumber()));
        }
    }
}
