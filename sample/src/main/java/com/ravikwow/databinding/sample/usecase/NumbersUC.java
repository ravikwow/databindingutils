package com.ravikwow.databinding.sample.usecase;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.DataSource;

import com.ravikwow.databinding.sample.model.ItemModel;
import com.ravikwow.databinding.sample.repository.Number;
import com.ravikwow.databinding.sample.repository.NumberDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ravikwow
 * Date: 29.03.19.
 */
public class NumbersUC {
    private static final int count = 500;
    private final NumberDao numberDao;
    private String search = "";

    public NumbersUC(NumberDao numberDao) {
        this.numberDao = numberDao;
    }

    public DataSource<Integer, ItemModel> getDataSource(String str) {
        search = str == null ? "" : str;
        return reloadDataSource();
    }

    public DataSource.Factory<Integer, ItemModel> getDataSourceFactory(String str) {
        search = str == null ? "" : str;
        return reloadDataSourceFactory();
    }

    public DataSource<Integer, ItemModel> reloadDataSource() {
        return numberDao.getDataSource(search)
                .map(input -> new ItemModel(input.getId(), input.getNumber()))
                .create();
    }

    public DataSource.Factory<Integer, ItemModel> reloadDataSourceFactory() {
        return numberDao.getDataSource(search)
                .map(input -> new ItemModel(input.getId(), input.getNumber()));
    }

    @NonNull
    public LiveData<List<ItemModel>> get() {
        return Transformations.map(numberDao.get(), numbers -> {
            List<ItemModel> itemModels = new ArrayList<>();
            for (Number number : numbers) {
                ItemModel itemModel = new ItemModel((int) number.getId(), number.getNumber());
                itemModels.add(itemModel);
            }
            return itemModels;
        });
    }

    public void generate(Consumer<Object> consumer) {
        new Thread(() -> {
            numberDao.generate(count);
            if (consumer != null) consumer.accept(null);
        }).start();
    }

    public void fillIfEmpty() {
        new Thread(() -> numberDao.fillIfEmpty(count)).start();
    }

    public int setRandom(final long id) {
        Random random = new Random(System.currentTimeMillis());
        final int newValue = random.nextInt(count * 5);
        new Thread(() -> {
            Number number = new Number(id, newValue);
            numberDao.update(number);
        }).start();
        return newValue;
    }
}
