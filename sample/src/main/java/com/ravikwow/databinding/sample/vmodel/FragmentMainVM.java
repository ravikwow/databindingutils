package com.ravikwow.databinding.sample.vmodel;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;

import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.utils.SingleLiveEvent;
import com.ravikwow.databinding.vm.ToolbarFragmentVM;

public class FragmentMainVM extends ToolbarFragmentVM {
    private final SingleLiveEvent<Void> startNumbersDataSourcEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> startNumbersEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> startNumbersKEvent = new SingleLiveEvent<>();

    public FragmentMainVM(@NonNull Application application) {
        super(application);
        title.set(R.string.app_name);
    }

    public SingleLiveEvent<Void> getStartNumbersEvent() {
        return startNumbersEvent;
    }

    public SingleLiveEvent<Void> getStartNumbersKEvent() {
        return startNumbersKEvent;
    }

    public SingleLiveEvent<Void> getStartNumbersDataSourcEvent() {
        return startNumbersDataSourcEvent;
    }

    public void numbersDataSourceClick(View view) {
        startNumbersDataSourcEvent.call();
    }

    public void numbersClick(View view) {
        startNumbersEvent.call();
    }

    public void numbersKClick(View view) {
        startNumbersKEvent.call();
    }
}
