package com.ravikwow.databinding.sample.di;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ravikwow
 * Date: 29.03.19.
 */
@Module
public class ContextModule {
    Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context context() {
        return context.getApplicationContext();
    }
}
