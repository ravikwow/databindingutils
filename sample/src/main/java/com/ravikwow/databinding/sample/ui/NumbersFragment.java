package com.ravikwow.databinding.sample.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ravikwow.databinding.sample.R;
import com.ravikwow.databinding.sample.adapter.NumbersAdapter;
import com.ravikwow.databinding.sample.databinding.FragmentListBinding;
import com.ravikwow.databinding.sample.di.ContextModule;
import com.ravikwow.databinding.sample.di.DaggerFragmentsComponent;
import com.ravikwow.databinding.sample.di.NumbersModule;
import com.ravikwow.databinding.sample.usecase.NumbersUC;
import com.ravikwow.databinding.sample.vmodel.FragmentListVM;
import com.ravikwow.databinding.ui.ToolbarFragment;

import javax.inject.Inject;

public class NumbersFragment extends ToolbarFragment<FragmentListBinding, FragmentListVM> {
    @Inject
    protected NumbersAdapter adapter;
    @Inject
    protected NumbersUC numbersUC;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerFragmentsComponent.builder()
                .contextModule(new ContextModule(getContext()))
                .numbersModule(new NumbersModule(viewModel, this))
                .build().inject(this);
        numbersUC.fillIfEmpty();
        numbersUC.get().observe(this, itemModels -> adapter.setItems(itemModels));
        viewModel.getRefreshEvent().observe(this, aVoid -> numbersUC.generate(null));
        viewModel.getSearchEvent().observe(this, search -> adapter.setFilter(type -> {
            String number = Integer.toString(type.getNumber());
            return number.contains(search);
        }));
        viewModel.getItemClickEvent().observe(this, model -> {
            Toast.makeText(getContext(), getString(R.string.item_click, model.getNumber()), Toast.LENGTH_SHORT).show();
        });
        viewModel.getItemLongClickEvent().observe(this, model -> {
            int oldNumber = model.getNumber();
            int newNumber = numbersUC.setRandom(model.getId());
            Toast.makeText(getContext(), getString(R.string.item_long_click, oldNumber, newNumber), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recyclerView.setAdapter(adapter);
    }
}
