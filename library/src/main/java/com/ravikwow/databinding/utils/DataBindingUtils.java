package com.ravikwow.databinding.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.ViewDataBinding;

/**
 * Created by ravikwow
 * Date: 30.11.2016
 */
public class DataBindingUtils {
    public static ViewDataBinding getViewDataBinding(int resId, LayoutInflater inflater, ViewGroup container) {
        return androidx.databinding.DataBindingUtil.inflate(inflater, resId, container, false, androidx.databinding.DataBindingUtil.getDefaultComponent());
    }

    public static int getLayoutResId(String name, Context context) {
        final int indexBinding = name.lastIndexOf("Binding");
        name = indexBinding < 0 ? name : name.substring(0, indexBinding);
        name = name.substring(0, 1).toLowerCase() + name.substring(1);
        name = TextUtils.join("_", name.split("(?=\\p{Upper})")).toLowerCase();
        return context.getResources().getIdentifier(name, "layout", context.getPackageName());
    }
}
