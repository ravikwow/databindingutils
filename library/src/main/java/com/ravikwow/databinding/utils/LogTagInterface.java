package com.ravikwow.databinding.utils;

/**
 * Created by ravikwow
 * Date: 29.10.2016.
 */
public interface LogTagInterface {
    String getLogTag();
//    Class<?> enclosingClass = getClass().getEnclosingClass();
//    if (enclosingClass != null) {
//        return enclosingClass.getSimpleName();
//    } else {
//        return getClass().getSimpleName();
//    }
}
