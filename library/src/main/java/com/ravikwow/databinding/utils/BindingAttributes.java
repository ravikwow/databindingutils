package com.ravikwow.databinding.utils;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

import androidx.databinding.BindingAdapter;

/**
 * Created by ravikwow
 * Date: 17.03.17.
 */
public class BindingAttributes {
    private static HashMap<String, Typeface> typefaceHashMap = new HashMap<>();

    @BindingAdapter({"visibility"})
    public static void setVisibility(View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("typeface")
    public synchronized static void setTypeface(TextView textView, String font) {
        Typeface tf = typefaceHashMap.get(font);
        if (tf == null) {
            tf = Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + font);
            typefaceHashMap.put(font, tf);
        }
        textView.setTypeface(tf);
    }
}
