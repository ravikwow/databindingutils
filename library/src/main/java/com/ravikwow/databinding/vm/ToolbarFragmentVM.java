package com.ravikwow.databinding.vm;

import android.app.Application;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableInt;

import com.ravikwow.databinding.R;
import com.ravikwow.databinding.ui.VM;

/**
 * Created by ravikwow
 * Date: 15.12.18.
 */
public class ToolbarFragmentVM extends VM {
    public final ObservableInt title = new ObservableInt(R.string.empty);
    public final ObservableInt menu = new ObservableInt(R.menu.empty);

    public ToolbarFragmentVM(@NonNull Application application) {
        super(application);
    }

    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}

