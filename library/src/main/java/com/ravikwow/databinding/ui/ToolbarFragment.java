package com.ravikwow.databinding.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ravikwow.databinding.R;
import com.ravikwow.databinding.databinding.FragmentToolbarBinding;
import com.ravikwow.databinding.utils.DataBindingUtils;
import com.ravikwow.databinding.vm.ToolbarFragmentVM;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

/**
 * Created by ravikwow
 * Date: 15.12.18.
 */
public class ToolbarFragment<T extends ViewDataBinding, VM extends ToolbarFragmentVM> extends DataBindingFragment<T, VM> {
    protected FragmentToolbarBinding bindingToolbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View content = super.onCreateView(inflater, container, savedInstanceState);
        //noinspection unchecked
        bindingToolbar = (FragmentToolbarBinding) DataBindingUtils.getViewDataBinding(R.layout.fragment_toolbar, inflater, container);
        bindingToolbar.setViewModel(viewModel);
        bindingToolbar.container.addView(content);
        return bindingToolbar.getRoot();
    }
}
