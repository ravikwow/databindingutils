package com.ravikwow.databinding.ui;

import android.os.Bundle;
import android.util.Log;

import com.ravikwow.databinding.utils.LogTagInterface;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by ravikwow
 * Date: 13.12.18.
 */
public class LogLifeCycleActivity extends AppCompatActivity implements LogTagInterface {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logLifeCycle("onCreate");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        logLifeCycle("onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        logLifeCycle("onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        logLifeCycle("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        logLifeCycle("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logLifeCycle("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logLifeCycle("onDestroy");
    }

    @Override
    public String getLogTag() {
        Class<?> enclosingClass = getClass().getEnclosingClass();
        if (enclosingClass != null) {
            return enclosingClass.getSimpleName();
        } else {
            return getClass().getSimpleName();
        }
    }

    protected void logLifeCycle(String text) {
        Log.d("ActivitiesLifeCycle", getLogTag() + ": " + text);
    }
}
