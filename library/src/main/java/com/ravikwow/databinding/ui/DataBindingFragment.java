package com.ravikwow.databinding.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.ravikwow.databinding.utils.DataBindingUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by ravikwow
 * Date: 15.12.18.
 */
public class DataBindingFragment<T extends ViewDataBinding, VM extends com.ravikwow.databinding.ui.VM> extends LogLifeCycleFragment {
    protected T binding;
    protected VM viewModel;

    protected static int getLayoutResId(DataBindingFragment fragment) {
        ParameterizedType typeGeneric = ((ParameterizedType) fragment.getClass().getGenericSuperclass());
        if (typeGeneric == null) {
            throw new RuntimeException("GenericSuperclass is null");
        }
        Type type = typeGeneric.getActualTypeArguments()[0];
        String name = ((Class) type).getSimpleName();
        Context context = fragment.getContext();
        if (context == null) {
            throw new RuntimeException("fragment.getContext() is null");
        }
        return DataBindingUtils.getLayoutResId(name, context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider.NewInstanceFactory vmFactory = getVMFactory();
        ViewModelProvider viewModelProvider = vmFactory == null ?
                ViewModelProviders.of(this) :
                ViewModelProviders.of(this, vmFactory);
        //noinspection unchecked,ConstantConditions
        Class<VM> vmClass = (Class<VM>)
                ((ParameterizedType) getClass()
                        .getGenericSuperclass())
                        .getActualTypeArguments()[1];
        viewModel = viewModelProvider.get(vmClass);
        getLifecycle().addObserver(viewModel);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //noinspection unchecked
        binding = (T) DataBindingUtils.getViewDataBinding(getLayoutResId(), inflater, container);
        binding.setVariable(com.ravikwow.databinding.BR.viewModel, viewModel);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().removeObserver(viewModel);
    }

    protected ViewModelProvider.NewInstanceFactory getVMFactory() {
        return null;
    }

    protected int getLayoutResId() {
        return getLayoutResId(this);
    }
}
