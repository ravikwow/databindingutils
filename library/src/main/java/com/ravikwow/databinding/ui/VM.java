package com.ravikwow.databinding.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleObserver;

public class VM extends AndroidViewModel implements LifecycleObserver {
    public VM(@NonNull Application application) {
        super(application);
    }
}
