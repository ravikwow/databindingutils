package com.ravikwow.databinding.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ravikwow.databinding.utils.LogTagInterface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by ravikwow
 * Date: 15.12.18.
 */
public class LogLifeCycleFragment extends Fragment implements LogTagInterface {
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        logLifeCycle("onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logLifeCycle("onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        logLifeCycle("onCreateView");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        logLifeCycle("onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        logLifeCycle("onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        logLifeCycle("onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        logLifeCycle("onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        logLifeCycle("onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        logLifeCycle("onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logLifeCycle("onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        logLifeCycle("onDetach");
    }

    @Override
    public String getLogTag() {
        Class<?> enclosingClass = getClass().getEnclosingClass();
        if (enclosingClass != null) {
            return enclosingClass.getSimpleName();
        } else {
            return getClass().getSimpleName();
        }
    }

    protected void logLifeCycle(String text) {
        Log.d("FragmentsLifeCycle", getLogTag() + ": " + text);
    }
}
