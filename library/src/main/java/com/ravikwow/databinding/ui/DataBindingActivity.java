package com.ravikwow.databinding.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.ravikwow.databinding.utils.DataBindingUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by ravikwow
 * Date: 13.12.18.
 */
public class DataBindingActivity<T extends ViewDataBinding, VM extends com.ravikwow.databinding.ui.VM> extends LogLifeCycleActivity {
    protected T binding;
    protected VM viewModel;

    protected static int getLayoutResId(DataBindingActivity activity) {
        ParameterizedType typeGeneric = ((ParameterizedType) activity.getClass().getGenericSuperclass());
        if (typeGeneric == null) {
            throw new RuntimeException("GenericSuperclass is null");
        }
        Type type = typeGeneric.getActualTypeArguments()[0];
        String name = ((Class) type).getSimpleName();
        return DataBindingUtils.getLayoutResId(name, activity);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider.NewInstanceFactory vmFactory = getVMFactory();
        ViewModelProvider viewModelProvider = vmFactory == null ?
                ViewModelProviders.of(this) :
                ViewModelProviders.of(this, vmFactory);
        //noinspection unchecked,ConstantConditions
        Class<VM> vmClass = (Class<VM>)
                ((ParameterizedType) getClass()
                        .getGenericSuperclass())
                        .getActualTypeArguments()[1];
//        if (vmClass == ViewModel.class) {
//            //noinspection unchecked
//            vmClass = (Class<VM>) AndroidViewModel.class;
//        }
        viewModel = viewModelProvider.get(vmClass);
        getLifecycle().addObserver(viewModel);
        //noinspection unchecked
        binding = (T) DataBindingUtils.getViewDataBinding(getLayoutResId(), getLayoutInflater(), null);
        binding.setVariable(com.ravikwow.databinding.BR.viewModel, viewModel);
        binding.setLifecycleOwner(this);
        setContentView(binding.getRoot());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getLifecycle().removeObserver(viewModel);
    }

    protected ViewModelProvider.NewInstanceFactory getVMFactory() {
        return null;
    }

    protected int getLayoutResId() {
        return getLayoutResId(this);
    }
}
