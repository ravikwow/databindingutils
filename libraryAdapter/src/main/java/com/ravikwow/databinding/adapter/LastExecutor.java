package com.ravikwow.databinding.adapter;

import android.os.AsyncTask;

import java.util.concurrent.Executor;

import androidx.annotation.NonNull;

/**
 * Created by ravikwow
 * Date: 12.01.17.
 */
public class LastExecutor implements Executor {
    @SuppressWarnings("WeakerAccess")
    public static final Executor LAST_EXECUTOR = new LastExecutor();
    private Runnable mLast;
    private Runnable mActive;

    public synchronized void execute(@NonNull final Runnable r) {
        mLast = new Runnable() {
            public void run() {
                try {
                    r.run();
                } finally {
                    scheduleNext();
                }
            }
        };
        if (mActive == null) {
            scheduleNext();
        }
    }

    private synchronized void scheduleNext() {
        if ((mActive = mLast) != null) {
            mLast = null;
            AsyncTask.THREAD_POOL_EXECUTOR.execute(mActive);
        }
    }
}
