package com.ravikwow.databinding.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.ViewDataBinding;

/**
 * Created by ravikwow
 * Date: 08.11.18.
 */
public class RVAdapterViewHolder<M, VM extends RVAdapterViewModel<M>>
        extends BindingViewHolder<ViewDataBinding> {
    private final VM vm;

    private RVAdapterViewHolder(ViewDataBinding binding, VM vm) {
        super(binding);
        this.vm = vm;
    }

    static <M, VM extends RVAdapterViewModel<M>> RVAdapterViewHolder<M, VM> create(VM vm, ViewGroup parent, int resId) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = getViewDataBinding(resId, inflater, parent);
        return new RVAdapterViewHolder<>(binding, vm);
    }

    @SuppressWarnings("WeakerAccess")
    public VM getVm() {
        return vm;
    }
}
