package com.ravikwow.databinding.adapter;

import android.view.View;

import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Created by ravikwow
 * Date: 20.05.16.
 */
public abstract class DataBindingRecyclerViewAdapter<VH extends BindingViewHolder> extends RecyclerView.Adapter<VH> {
    private View.OnClickListener onClickListener;
    private View.OnLongClickListener onLongClickListener;
    private View.OnCreateContextMenuListener onCreateContextMenuListener;
    private LifecycleOwner lifecycleOwner;

    public DataBindingRecyclerViewAdapter(LifecycleOwner lifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner;
    }

    @SuppressWarnings("unused")
    public void setOnItemCreateContextMenu(View.OnCreateContextMenuListener onCreateContextMenuListener) {
        this.onCreateContextMenuListener = onCreateContextMenuListener;
    }

    @SuppressWarnings("unused")
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @SuppressWarnings("unused")
    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    @Override
    public void onBindViewHolder(VH holder, int position, List<Object> payloads) {
        holder.binding.setVariable(com.ravikwow.databinding.adapter.BR.onClickListener, onClickListener);
        holder.binding.setVariable(com.ravikwow.databinding.adapter.BR.onLongClickListener, onLongClickListener);
        holder.binding.setVariable(com.ravikwow.databinding.adapter.BR.onCreateContextMenuListener, onCreateContextMenuListener);
        holder.binding.setLifecycleOwner(lifecycleOwner);
        super.onBindViewHolder(holder, position, payloads);
    }
}
