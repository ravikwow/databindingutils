package com.ravikwow.databinding.adapter;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

public abstract class DataBindingRecyclerViewPagedAdapter<T, VH extends BindingViewHolder>
        extends PagedListAdapter<T, VH> {
    protected LifecycleOwner lifecycleOwner;

    public DataBindingRecyclerViewPagedAdapter(@NonNull DiffUtil.ItemCallback<T> diffCallback, LifecycleOwner lifecycleOwner) {
        super(diffCallback);
        this.lifecycleOwner = lifecycleOwner;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position, @NonNull List<Object> payloads) {
        holder.binding.setLifecycleOwner(lifecycleOwner);
        super.onBindViewHolder(holder, position, payloads);
    }
}
