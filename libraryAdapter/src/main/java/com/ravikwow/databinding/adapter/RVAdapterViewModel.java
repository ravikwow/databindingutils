package com.ravikwow.databinding.adapter;

import androidx.annotation.CallSuper;

public class RVAdapterViewModel<M> {
    private M model;

    public M getModel() {
        return model;
    }

    @CallSuper
    public void bind(M model, int position) {
        this.model = model;
    }
}
