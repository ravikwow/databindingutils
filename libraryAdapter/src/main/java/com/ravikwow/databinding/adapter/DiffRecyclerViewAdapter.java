package com.ravikwow.databinding.adapter;

import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ravikwow
 * Date: 02.11.16.
 */
public abstract class DiffRecyclerViewAdapter<T, VH extends BindingViewHolder<ViewDataBinding>> extends DataBindingRecyclerViewAdapter<VH> {
    private final AsyncListDiffer<T> asyncListDiffer;
    private Collection<T> itemsAll;
    private Comparator<T> comparator;
    private DiffListener diffListener;
    private Filter<T> filter;

    @SuppressWarnings({"unused", "WeakerAccess"})
    public DiffRecyclerViewAdapter(DiffUtil.ItemCallback<T> compareCallbacks, LifecycleOwner lifecycleOwner) {
        super(lifecycleOwner);
        if (compareCallbacks == null) {
            throw new RuntimeException("DiffUtil.ItemCallback is null");
        }
        asyncListDiffer = new AsyncListDiffer<>(this, compareCallbacks);
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setFilter(Filter<T> filter) {
        this.filter = filter;
        if (itemsAll != null) {
            AsyncTaskItems<T> asyncTaskItems = new AsyncTaskItems<>(diffListener, comparator,
                    this.filter, asyncListDiffer);
            //noinspection unchecked
            asyncTaskItems.executeOnExecutor(LastExecutor.LAST_EXECUTOR, itemsAll);
        }
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setComparator(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setDiffListener(DiffListener diffListener) {
        this.diffListener = diffListener;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public T getItem(int position) {
        return asyncListDiffer.getCurrentList().get(position);
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public List<T> getItems() {
        return asyncListDiffer.getCurrentList();
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setItems(final Collection<T> newItems) {
        itemsAll = newItems;
        AsyncTaskItems<T> asyncTaskItems = new AsyncTaskItems<>(diffListener,
                comparator, filter, asyncListDiffer);
        //noinspection unchecked
        asyncTaskItems.executeOnExecutor(LastExecutor.LAST_EXECUTOR, itemsAll);
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public Collection<T> getAllItems() {
        return itemsAll == null ? null : Collections.unmodifiableCollection(itemsAll);
    }

    @Override
    public int getItemCount() {
        return asyncListDiffer.getCurrentList().size();
    }

    public interface DiffListener {
        void onPreCalcDiff();

        void onPostCalcDiff();

        void onPostDispatch();
    }

    public interface Filter<T> {
        boolean apply(T type);
    }
}
