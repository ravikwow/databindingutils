package com.ravikwow.databinding.adapter;

import android.os.AsyncTask;

import androidx.recyclerview.widget.AsyncListDiffer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ravikwow
 * Date: 01.03.18.
 */
class AsyncTaskItems<T> extends AsyncTask<Collection<T>, Void, List<T>> {
    private DiffRecyclerViewAdapter.DiffListener diffListener;
    private Comparator<T> comparator;
    private DiffRecyclerViewAdapter.Filter<T> filter;
    private AsyncListDiffer<T> asyncListDiffer;

    AsyncTaskItems(DiffRecyclerViewAdapter.DiffListener diffListener, Comparator<T> comparator,
                   DiffRecyclerViewAdapter.Filter<T> filter, AsyncListDiffer<T> asyncListDiffer) {
        this.diffListener = diffListener;
        this.comparator = comparator;
        this.filter = filter;
        this.asyncListDiffer = asyncListDiffer;
    }

    private static <T> ArrayList<T> filter(Collection<T> items, DiffRecyclerViewAdapter.Filter<T> filter) {
        ArrayList<T> result = new ArrayList<>();
        for (T element : items) {
            if (filter.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        if (diffListener != null) {
            diffListener.onPreCalcDiff();
        }
    }

    @SafeVarargs
    @Override
    protected final List<T> doInBackground(Collection<T>... data) {
        Collection<T> itemsAll = data[0];
        ArrayList<T> itemsNew = filter == null ? new ArrayList<>(itemsAll) : filter(itemsAll, filter);
        if (comparator != null) {
            Collections.sort(itemsNew, comparator);
        }
        return itemsNew;
    }

    @Override
    protected void onPostExecute(List<T> diffResult) {
        if (diffListener != null) {
            diffListener.onPostCalcDiff();
        }
        asyncListDiffer.submitList(diffResult);
        if (diffListener != null) {
            diffListener.onPostDispatch();
        }
    }
}
