package com.ravikwow.databinding.adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.DiffUtil;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public abstract class DataSourcePagedAdapter<M, VH extends BindingViewHolder>
        extends DataBindingRecyclerViewPagedAdapter<M, VH> {
    private final int pageSize;

    public DataSourcePagedAdapter(@NonNull DiffUtil.ItemCallback<M> diffCallback,
                                  LifecycleOwner lifecycleOwner,
                                  int pageSize) {
        super(diffCallback, lifecycleOwner);
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void submitDataSource(DataSource<Integer, M> dataSource) {
        @SuppressLint("RestrictedApi")
        FutureTask<PagedList<M>> future =
                new FutureTask<>(() -> new PagedList.Builder<>(dataSource, pageSize)
                        .setNotifyExecutor(ArchTaskExecutor.getMainThreadExecutor())
                        .setFetchExecutor(Executors.newSingleThreadExecutor())
                        .build());
        new Thread(future).start();
        try {
            submitList(future.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void submitDataSourceFactory(DataSource.Factory<Integer, M> dataSourceFactory) {
        LiveData<PagedList<M>> liveList = new LivePagedListBuilder<>(
                dataSourceFactory, pageSize).build();
        liveList.observe(lifecycleOwner, this::submitList);
    }
}
