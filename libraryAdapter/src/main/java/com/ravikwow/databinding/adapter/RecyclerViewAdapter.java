package com.ravikwow.databinding.adapter;

import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DiffUtil;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ravikwow
 * Date: 02.11.16.
 */
public abstract class RecyclerViewAdapter<M, VM extends RVAdapterViewModel<M>>
        extends DiffRecyclerViewAdapter<M, RVAdapterViewHolder<M, VM>> {
    private final int[] resIdsViews;
    private final HashMap<M, VM> mvm = new HashMap<>();

    @SuppressWarnings("unused")
    public RecyclerViewAdapter(DiffUtil.ItemCallback<M> compareCallbacks, LifecycleOwner lifecycleOwner, int... resIdsViews) {
        super(compareCallbacks, lifecycleOwner);
        this.resIdsViews = resIdsViews;
    }

    @NonNull
    @Override
    public RVAdapterViewHolder<M, VM> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return RVAdapterViewHolder.create(createViewModel(viewType), parent, resIdsViews[viewType]);
    }

    @Override
    public void onBindViewHolder(@NonNull RVAdapterViewHolder<M, VM> holder, int position) {
        M m = getItem(position);
        if (m != null) {
            VM vm = holder.getVm();
            mvm.put(m, vm);
            bindViewModel(m, vm, position);
            holder.binding.setVariable(com.ravikwow.databinding.adapter.BR.viewModel, vm);
        }
    }

    @Override
    public void setItems(Collection<M> newItems) {
        super.setItems(newItems);
        Collection<M> allModels = getAllItems();
        Set<M> vmModels = new HashSet<>(mvm.keySet());
        vmModels.removeAll(allModels);
        for (M model : vmModels) {
            mvm.remove(model);
        }
    }

    @SuppressWarnings("unused")
    public VM getViewModel(M m) {
        return mvm.get(m);
    }

    @SuppressWarnings("unused")
    public Collection<VM> getViewModels() {
        return mvm.values();
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    protected VM createViewModel(int viewType) {
        VM vm = null;
        try {
            //noinspection unchecked,ConstantConditions
            Class<VM> vmClass = (Class<VM>)
                    ((ParameterizedType) getClass()
                            .getGenericSuperclass())
                            .getActualTypeArguments()[1];
            Constructor<VM> vmConstructor = vmClass.getConstructor();
            vm = vmConstructor.newInstance();
        } catch (Throwable ignored) {
        }
        return vm;
    }

    @CallSuper
    protected void bindViewModel(M m, VM vm, int position) {
        vm.bind(m, position);
    }
}
