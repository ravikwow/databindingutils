package com.ravikwow.databinding.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ravikwow
 * Date: 20.05.16.
 */
public class BindingViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    public final T binding;

    @SuppressWarnings("WeakerAccess")
    protected BindingViewHolder(T binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    @SuppressWarnings("WeakerAccess")
    protected static ViewDataBinding getViewDataBinding(int resId, LayoutInflater inflater, ViewGroup container) {
        return androidx.databinding.DataBindingUtil.inflate(inflater, resId, container, false, androidx.databinding.DataBindingUtil.getDefaultComponent());
    }
}

