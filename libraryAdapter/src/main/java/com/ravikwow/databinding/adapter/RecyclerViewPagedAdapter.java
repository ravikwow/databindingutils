package com.ravikwow.databinding.adapter;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.lifecycle.LifecycleOwner;
import androidx.paging.DataSource;
import androidx.paging.PagedList;
import androidx.paging.PositionalDataSource;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

@SuppressWarnings("unused")
public abstract class RecyclerViewPagedAdapter<M, VM extends RVAdapterViewModel<M>>
        extends RecyclerViewDataSourceAdapter<M, VM> {
    private final int pageSize;
    private final GetData<M> getData;
    private final PagedList.BoundaryCallback<M> boundaryCallback;

    @SuppressWarnings("WeakerAccess")
    protected RecyclerViewPagedAdapter(@NonNull DiffUtil.ItemCallback<M> diffCallback,
                                       @NonNull GetData<M> getData,
                                       int pageSize,
                                       @Nullable LifecycleOwner lifecycleOwner,
                                       @Nullable PagedList.BoundaryCallback<M> boundaryCallback,
                                       int... resIdsViews) {
        super(diffCallback, lifecycleOwner, pageSize, resIdsViews);
        this.getData = getData;
        this.pageSize = pageSize;
        this.boundaryCallback = boundaryCallback;
        reload();
    }

    protected RecyclerViewPagedAdapter(@NonNull DiffUtil.ItemCallback<M> diffCallback,
                                       @NonNull GetData<M> getData,
                                       int pageSize,
                                       @Nullable LifecycleOwner lifecycleOwner,
                                       int... resIdsViews) {
        this(diffCallback, getData, pageSize, lifecycleOwner, null, resIdsViews);
    }

    protected RecyclerViewPagedAdapter(@NonNull DiffUtil.ItemCallback<M> diffCallback,
                                       @NonNull GetData<M> getData,
                                       int pageSize,
                                       int... resIdsViews) {
        this(diffCallback, getData, pageSize, null, null, resIdsViews);
    }

    @SuppressWarnings("WeakerAccess")
    public void reload() {
        MyPositionalDataSource<M> dataSource = new MyPositionalDataSource<>(getData);
        submitDataSource(dataSource);
    }

    @Override
    public void submitDataSource(DataSource<Integer, M> dataSource) {
        @SuppressLint("RestrictedApi")
        FutureTask<PagedList<M>> future =
                new FutureTask<>(() -> new PagedList.Builder<>(dataSource, new PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
                        .setPageSize(pageSize).build())
                        .setBoundaryCallback(boundaryCallback)
                        .setNotifyExecutor(ArchTaskExecutor.getMainThreadExecutor())
                        .setFetchExecutor(Executors.newSingleThreadExecutor())
                        .build());
        new Thread(future).start();
        try {
            submitList(future.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public interface GetData<M> {
        List<M> getData(long startPosition, int loadSize);
    }

    private static class MyPositionalDataSource<M> extends PositionalDataSource<M> {
        private final GetData<M> data;

        private MyPositionalDataSource(GetData<M> data) {
            this.data = data;
        }

        @Override
        public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<M> callback) {
            Log.d(DataSourcePagedAdapter.class.getSimpleName(), "loadInitial, requestedStartPosition = " + params.requestedStartPosition +
                    ", requestedLoadSize = " + params.requestedLoadSize);
            new Thread(() -> {
                List<M> result = data.getData(params.requestedStartPosition, params.requestedLoadSize);
                callback.onResult(result, params.requestedStartPosition);
            }).start();
        }

        @Override
        public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<M> callback) {
            Log.d(DataSourcePagedAdapter.class.getSimpleName(), "loadRange, startPosition = " + params.startPosition + ", loadSize = " + params.loadSize);
            new Thread(() -> {
                List<M> result = data.getData(params.startPosition, params.loadSize);
                callback.onResult(result);
            }).start();
        }
    }
}
